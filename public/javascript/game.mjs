import MarkupHelper from "./markupHelper.mjs";

class Game {
  constructor() {
    this.markupHelper = new MarkupHelper();
    this.username = sessionStorage.getItem("username");
    if (!this.username) this.reLogin();
    this.socket = io("", { query: { username: this.username } });
    this.createEventListeners();
  }

  reLogin() {
    sessionStorage.clear();
    window.location.replace("/login");
  }

  createEventListeners() {
    this.socket.on("ERROR_NOT_UNIQUE", () => {
      alert("Username is already taken");
      this.reLogin()
    });

    this.socket.on("SHOW_ROOM", (rooms) => this.displayRooms(rooms));
    this.socket.on("SHOW_GAMEPAGE", (roomname) => this.displayGame(roomname));
    this.socket.on("SHOW_USERS", (users) => this.displayUsers(users));
    this.socket.on("USER_STATUS", (user) => this.markupHelper.updateUserStatus(user));
    this.socket.on("GAME_STARTED", (data) => this.prepareGame(data));
    this.socket.on("PROGRESS_UPDATE", (data) => this.markupHelper.updateUserProgressBar(data));
    this.socket.on("GAME_OVER", (finishers) => this.finishGame(finishers));
    this.socket.on("USER_ERROR", (error) => alert(error));
  }

  displayUsers(users) {
    this.markupHelper.renderUsers(users, this.username);
  }

  displayRooms(rooms) {
    this.markupHelper.clearRooms();
    for (let room of rooms) {
      this.markupHelper.renderRoom(room.name, room.players, () => this.joinRoom(room.name))
    }
  }

  displayGame(roomname) {
    this.markupHelper.hideRooms();
    this.markupHelper.renderGame(
      roomname,
      () => this.userReady(),
      () => this.userLeave()
    );
  }

  prepareGame(data) {
    this.gameTimer = data.gameTimer;
    this.markupHelper.hideReadyBtn();
    this.markupHelper.hideBackBtn();
    this.startWaitTimer(data.waitTimer);
    this.downloadText(data.textId);
  }

  downloadText(id) {
    fetch(`/game/texts/${id}`)
      .then(response => response.json())
      .then(data => data.text)
      .then(text => this.gametext = text)
      .catch(error => console.log(error))
  }

  startWaitTimer(time) {
    this.markupHelper.displayTimer();
    this.markupHelper.updateTimer(time);
    const timerID = setInterval(() => {
      this.markupHelper.updateTimer(--time);
      if (time <= 0) {
        clearInterval(timerID);
        this.markupHelper.hideTimer();
        this.startGame()
        return;
      }
    }, 1000)
  }

  startGameTimer(time) {
    this.markupHelper.displayGameTimer();
    this.markupHelper.updateGameTimer(time);
    const timerID = setInterval(() => {
      this.markupHelper.updateGameTimer(--time);
      if (time <= 0) {
        clearInterval(timerID);
        this.markupHelper.hideGameTimer();
        return;
      }
    }, 1000)
  }

  startGame() {
    this.startGameTimer(this.gameTimer)
    document.addEventListener("keypress", (event) => {
      this.keyPressed(event.key)
    })

    this.markupHelper.renderGameText(this.gametext)
    this.textSymbols = [...this.gametext];
    this.totalSymbols = this.textSymbols.length;
    this.textSymbols.reverse();
  }

  displayResults(finishers) {
    let message = "";

    for (let index = 0; index < finishers.length; index++) {
      const element = finishers[index];
      message += `${index + 1}# ${element}\n`;
    }
    alert(message);
  }

  finishGame(finishers) {
    document.removeEventListener("keypress", (event) => {
      this.keyPressed(event.key)
    })
    this.displayResults(finishers);
  }

  createRoom() {
    
    const roomName = prompt("Enter the name ");
    if(roomName) this.socket.emit("ADD_ROOM", roomName);
  }

  joinRoom(roomname) {
    this.socket.emit("JOIN_ROOM", { roomname, username: this.username });
  }

  keyPressed(key) {
    const firstSymbol = this.textSymbols[this.textSymbols.length - 1];
    if (firstSymbol === key) {
      const completedSymbol = this.textSymbols.pop();
      this.markupHelper.fillCompletedSymbols(completedSymbol);

      const currentTextSymbols = this.textSymbols.length;
      const progress = (1 - currentTextSymbols / this.totalSymbols) * 100;

      this.socket.emit("USER_PROGRESS_CHANGED", progress);
    }
  }

  userReady() {
    this.socket.emit("USER_READY")
  }

  userLeave() {
    this.socket.emit("USER_LEFT_ROOM")
    window.location.reload();
  }

}


export default Game;