import { createElement, removeClass, addClass } from "./helper.mjs";

class MarkupHelper {
  constructor() {
    this.redyBtnClicked = false;
  }
  hideRooms() {
    const roomPage = document.getElementById("rooms-page");
    addClass(roomPage, "display-none");
  }

  hideReadyBtn() {
    const readyBtn = document.getElementById("ready-btn");
    addClass(readyBtn, "display-none");

  }

  hideBackBtn() {
    const backBtn = document.getElementById("back-btn");
    addClass(backBtn, "display-none");
  }
  displayBackBtn() {
    const backBtn = document.getElementById("back-btn");
    removeClass(backBtn, "display-none");
  }
  displayTimer() {
    const timer = document.getElementById("timer");
    removeClass(timer, "display-none");

  }
  hideTimer() {
    const timer = document.getElementById("timer");
    addClass(timer, "display-none");
  }

  updateTimer(time) {
    const timer = document.getElementById("timer");
    timer.innerText = time;
  }

  displayGameTimer() {
    const timer = document.getElementById("game-timer");
    removeClass(timer, "display-none");

  }

  hideGameTimer() {
    const timer = document.getElementById("game-timer");
    addClass(timer, "display-none");
  }

  updateGameTimer(time) {
    const timer = document.getElementById("game-timer");
    timer.innerText = time;
  }
  updateUserProgressBar(data) {
    const userContainer = document.getElementById(data.username).closest("div");
    const progressBar = userContainer.lastElementChild;
    progressBar.value = data.progress;

    if(data.progress >= 100){
      progressBar.style.backgroundColor = "green";
    }
  }
  updateUserStatus(user) {
    const userElement = document.getElementById(user.username);
    userElement.querySelector(".user-status").innerText = (user.ready) ? " ready" : "";
  }

  renderUsers(users, currentUser) {
    const players = document.getElementById("players-status");
    players.innerText = "";

    const usersElms = users.map(user => this.getUserElement(user.username, user.username === currentUser));
    players.append(...usersElms);
  }
  getUserElement(username, isCurrent) {
    const div = createElement({
      tagName: "div",
      className: "user-wrapper"
    });

    const p = createElement({
      tagName: "p",
      className: "user-name",
      attributes: {
        id: username
      }
    });

    const progressBar = createElement({
      tagName: "progress",
      className: "progress-bar",
      attributes: {
        max: 100,
        value: 0
      }
    });

    const spanStatus = createElement({
      tagName: "span",
      className: "user-status"
    });

    p.innerText = isCurrent ? username + " (you)" : username;

    p.append(spanStatus);
    div.append(p, progressBar);
    return div;
  }
  fillCompletedSymbols(completedSymbol) {
    const spanCompleted = document.getElementById("completed");
    const spanNotCompleted = document.getElementById("not-completed");
    spanCompleted.innerText += completedSymbol;
    spanNotCompleted.innerText = spanNotCompleted.textContent.substr(1);
  }
  renderGameText(text) {
    const gameArea = document.querySelector(".game-area");
    const textContainer = createElement({
      tagName: "div",
      className: "text-container"
    });

    const p = createElement({
      tagName: "div",
      className: "text"
    });

    const span = createElement({
      tagName: "span",
      attributes: { id: "completed" }
    });

    const spanNotComplete = createElement({
      tagName: "span",
      attributes: { id: "not-completed" }
    });

    spanNotComplete.innerText = text;

    p.append(span, spanNotComplete);
    textContainer.append(p);
    gameArea.append(textContainer)
  }
  renderGame(roomname, onReadyCallBack, onLeaveCallBack) {
    const gamePage = document.getElementById("game-page");
    gamePage.innerText = ""
    removeClass(gamePage, "display-none");

    const gameContainer = createElement({
      tagName: "div",
      className: "game-container"
    });

    const sideBar = createElement({
      tagName: "div",
      className: "side-bar"
    });

    const gameArea = createElement({
      tagName: "div",
      className: "game-area"
    });

    const btnBackToRooms = createElement({
      tagName: "button",
      attributes: { id: "back-btn" }
    });

    const readyBtn = createElement({
      tagName: "button",
      attributes: { id: "ready-btn" }
    });

    const timer = createElement({
      tagName: "span",
      className: "display-none",
      attributes: { id: "timer" }
    });
    const gameTimer = createElement({
      tagName: "span",
      className: "display-none",
      attributes: { id: "game-timer" }
    })

    const header = createElement({
      tagName: "h2",
      className: "game-name",
    });

    const players = createElement({
      tagName: "div",
      className: "players",
      attributes: { id: "players-status" }
    });

    btnBackToRooms.addEventListener("click", onLeaveCallBack);

    readyBtn.addEventListener("click", () => {
      const readyBtn = document.getElementById("ready-btn");
      this.redyBtnClicked = !this.redyBtnClicked;
      readyBtn.innerText = (this.redyBtnClicked) ? "Not ready" : "Ready";
      onReadyCallBack()
    });

    header.innerText = roomname;
    btnBackToRooms.innerText = "Back To Rooms"
    readyBtn.innerText = "Ready";

    gameArea.append(readyBtn, timer, gameTimer);
    sideBar.append(header, btnBackToRooms, players);
    gameContainer.append(sideBar, gameArea);
    gamePage.append(gameContainer);
  }
  clearRooms(){
    document.querySelector(".rooms").innerText = ""
  }
  renderRoom(roomname, players, onJoinCallBack) {
    const roomWrapper = createElement({
      tagName: "div",
      className: "room",
      attributes: { id: roomname }
    });

    const span = createElement({
      tagName: "span",
      className: "users-connected",
    });
    //todo: add player limit count
    const header = createElement({
      tagName: "h2",
      className: "room-name",
    });

    const button = createElement({
      tagName: "button",
      className: "join-btn",
    });


    button.addEventListener("click", onJoinCallBack)

    button.innerText = "Join";
    span.innerText = `${players} user(s) connected`;
    header.innerText = `${roomname}`

    roomWrapper.append(span, header, button);
    document.querySelector(".rooms").append(roomWrapper);
  }


}

export default MarkupHelper